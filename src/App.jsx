import { useEffect, useState } from 'react'
import './App.css'
import axios from 'axios'
import authorization from './Authorization/ApiKey'
import Homepage from './Components/Homepage'
import { Routes,Route } from 'react-router-dom'
import AllList from './Components/Lists/AllList'
import ResponsiveAppBar from './Components/Navbar/Navbar'
import ErrorPage from './Error';

function App() {
  const [board,setBoard] = useState([]);

  useEffect(()=>{
    axios.get(`https://api.trello.com/1/members/me/boards?&key=${authorization.key}&token=${authorization.token}`).then((res)=>{
      setBoard(res.data);
    });
  },[]);

  return (
    <>
    <ResponsiveAppBar/>
    <Routes>
      <Route
      path='*'
      element={
        
        <ErrorPage/>
      }>
        
      </Route>
      <Route
      path='/'
      element={

        <Homepage board={board} setBoard={setBoard}/>
      }
      >

      </Route>
      <Route path="/boards/:id" element={<AllList board={board}/>}>
      </Route>
    </Routes>
    </>
  )
}

export default App





