import React from "react";
import { Typography, Container } from "@mui/material";

function ErrorPage() {
  return (
    <Container maxWidth="sm">
      <div style={{ textAlign: "center", marginTop: "50px" }}>
        <Typography variant="h4" component="h1" gutterBottom>
          Error
        </Typography>
        <Typography variant="body1">
          Sorry, we couldn't fetch at the moment. Please try again
          later.
        </Typography>
      </div>
    </Container>
  );
}

export default ErrorPage;
