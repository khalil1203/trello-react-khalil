import { Button, Input } from "@mui/material";
import React, { useState } from "react";
import authorization from "../../Authorization/ApiKey";
import axios from "axios";

const AddItems = ({ checklistName, checklistId, setItems, items }) => {
  const [itemName, setItemName] = useState("");

  function change(e) {
    setItemName(e);
  }

  async function addItem() {
    if (itemName.length) {
      try {
        const res = await axios.post(
          `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${itemName}&key=${authorization.key}&token=${authorization.token}`
        );
        setItems([...items, res.data]);
        setItemName("");
      } catch (e) {
        console.error("Failed to post new CheckList Item", e);
      }
    }
  }

  return (
    <div>
      <form action="" className="item-form">
        <Input
          type="text"
          value={itemName}
          placeholder="Give title for Item..."
          onChange={(e) => change(e.target.value)}
        />
        <Button onClick={addItem}>Add Item</Button>
      </form>
    </div>
  );
};

export default AddItems;
//
