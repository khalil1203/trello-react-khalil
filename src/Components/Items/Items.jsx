import React, { useEffect, useState } from "react";
import AddItems from "./AddItems";
import "./Item.css";
import authorization from "../../Authorization/ApiKey";
import axios from "axios";
import { Box } from "@mui/material";
import DeleteItem from "./DeleteItem";
import ProgressBar from "./ProgressBar";

const Items = ({ checklistId, checklist }) => {
  const [items, setItems] = useState([]);
  const [curProgress, setCurProgress] = useState(0);
  const [current, setCurrent] = useState(true);

  async function updateItems(itemId, cur) {
    try {
      const res = await axios.put(
        `https://api.trello.com/1/cards/${checklist.idCard}/checkItem/${itemId}?key=${authorization.key}&token=${authorization.token}`,
        { state: cur }
      );
    } catch (error) {
      console.log("error from update items", error);
    }
  }

  function isCheckFunc(itemId) {
    const itemArr = items.map((entry) => {
      if (entry.id === itemId) {
        let cur = entry.state === "complete" ? "incomplete" : "complete";
        updateItems(itemId, cur);
        return {
          ...entry,
          state: cur,
        };
      }
      return entry;
    });
    setItems(itemArr);
  }

  
  let checkedItems =  items.filter((entry) => {
     return (entry.state == "complete")
  });

  useEffect(() => {
    try {
      axios
        .get(
          `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${authorization.key}&token=${authorization.token}`
        )
        .then((res) => {
          setItems(res.data);
        });
    } catch (error) {}
  }, []);
  return (
    <div>
      <ProgressBar curProgress={checkedItems.length} items={items} />
      <AddItems checklistId={checklistId} items={items} setItems={setItems} />
      <Box className="items-div">
        {items.map((entry) => {
          return (
            <Box key={entry.id}>
              <Box
                key={entry.id}
                sx={{
                  p: 1,
                  display: "flex",
                  justifyContent: "space-between",
                  borderRadius: 1,
                  bgcolor: "#F1F2F4",
                  width: "320px",
                  justifySelf: "center",
                }}
              >
                <Box sx={{ display: "flex", gap: 1 }}>
                  <input
                    type="checkbox"
                    onClick={() => {
                      isCheckFunc(entry.id);
                    }}
                    defaultChecked={entry.state === "complete"}
                  ></input>
                  <h4>{entry.name}</h4>
                </Box>
                <DeleteItem
                  checklistId={checklistId}
                  itemId={entry.id}
                  items={items}
                  setItems={setItems}
                />
              </Box>
            </Box>
          );
        })}
      </Box>
    </div>
  );
};

export default Items;
