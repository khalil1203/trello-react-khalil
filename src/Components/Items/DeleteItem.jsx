import axios from "axios";
import React from "react";
import authorization from "../../Authorization/ApiKey";

const DeleteItem = ({ itemId, items, setItems, checklistId }) => {
  async function deleteItemFunc() {
    try {
      await axios.delete(
        `https://api.trello.com/1/checklists/${checklistId}/checkItems/${itemId}?key=${authorization.key}&token=${authorization.token}`
      );
      setItems(items.filter((item) => item.id !== itemId));
    } catch (e) {
      console.error("Failed to Delete new CheckList Item", e);
    }
  }

  return (
    <div>
      <i className="fa-solid fa-trash" onClick={deleteItemFunc}></i>
    </div>
  );
};

export default DeleteItem;
