import React from 'react'

const BoardHeader = ({currentBoard}) => {
    console.log(currentBoard)

  return (
    <div className='board-header'>
      <h2>{currentBoard[0].name}</h2>
    </div>
  )
}

export default BoardHeader
