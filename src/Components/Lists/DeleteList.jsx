import * as React from "react";
import Box from "@mui/material/Box";
import Popper from "@mui/material/Popper";
import CloseIcon from "@mui/icons-material/Close";
import { Button } from "@mui/material";

export default function SimplePopper({ deleteList, listid }) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  return (
    <div>
      <CloseIcon
        aria-describedby={id}
        onClick={handleClick}
        style={{ cursor: "pointer" }}
      />
      <Popper id={id} open={open} anchorEl={anchorEl}>
        <Box
          sx={{ border: 1, p: 1, bgcolor: "background.paper", display: "flex",flexDirection:"column",justifyContent:"center",gap:1 }}
        >
          <h3>Do you want to delete this list?</h3>
          <Button variant="contained" sx={{p:1}}  onClick={() => deleteList(listid)}>
            Yes
          </Button>
          <Button variant="text" sx={{p:1}}  onClick={handleClick}>
            No
          </Button>
        </Box>
      </Popper>
    </div>
  );
}
