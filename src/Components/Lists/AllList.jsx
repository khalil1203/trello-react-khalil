import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "./AllList.css";
import Loader from "../Loading/Loader.jsx";
import authorization from "../../Authorization/ApiKey";
import AddList from "./AddList";
import AllCards from "../Cards/AllCards";
import BoardHeader from "./BoardHeader";
import SimplePopper from "./DeleteList";
import ErrorPage from "../../Error";
import { Box, Container } from "@mui/material";

const AllList = ({ board }) => {
  const [lists, setLists] = useState([]);
  const [newListName, setNewListName] = useState("");
  const id = useParams().id;
  const [errorStatus, setErrorStatus] = useState(false);
  let currentBoard = [board.find((entry) => entry.id == id)];

  function changeList(e) {
    setNewListName(e);
  }

  async function deleteList(e) {
    try {
      await axios.put(
        `https://api.trello.com/1/lists/${e}/closed?key=${authorization.key}&token=${authorization.token}`,
        {
          value: true,
        }
      );
      setLists(lists.filter((list) => list.id !== e));
    } catch (error) {
      console.log(error);
    }
  }

  async function addList() {
    if (newListName.length) {
      try {
        const response = await axios.post(
          `https://api.trello.com/1/lists?name=${newListName}&idBoard=${id}&key=${authorization.key}&token=${authorization.token}`
        );
        setNewListName("");
        setLists([...lists, response.data]);
      } catch (error) {
        console.log(error);
      }
    }
  }

  useEffect(() => {
    try {
      axios
        .get(
          `https://api.trello.com/1/boards/${id}/lists?&key=${authorization.key}&token=${authorization.token}`
        )
        .then((res) => {
          setLists(res.data);
          setErrorStatus(false);
        })
        .catch((error) => {
          setErrorStatus(true);
        });
    } catch (error) {
      console.log(error);
    }
  }, []);

  if (!board.length) return <Loader />;

  return (
    <div>
      {errorStatus ? (
        <ErrorPage />
      ) : (
        <div>
          <BoardHeader currentBoard={currentBoard} />
          <Container
            className="listpage"
            sx={{
              p: 2,
              display: "flex",
              gap: 10,
              position: "absolute",
              backgroundImage: `url(${currentBoard[0].prefs.backgroundImage})`,
            }}
          >
            <Box
              sx={{
                p: 2,
                display: "flex",
                gap: 2,
                position: "absolute",
                boxSizing: "border-box",
                alignItems: "flex-start",
              }}
            >
              {lists.map((entry) => {
                return (
                  <Box
                    sx={{
                      display: "flex",
                      display: "inline-block",
                      flexDirection: "column",
                      gap: 3,
                      width: 300,
                      minHeight: 50,
                      bgcolor: "#F1F2F4",
                      position: "relative",
                      p: 2,
                      borderRadius: 4,
                    }}
                    key={entry.id}
                  >
                    <Box
                      sx={{ display: "flex", justifyContent: "space-between" }}
                    >
                      <h3>{entry.name}</h3>
                      <Box>
                        <SimplePopper
                          deleteList={deleteList}
                          listid={entry.id}
                        />
                      </Box>
                    </Box>
                    <AllCards listid={entry.id}  />
                  </Box>
                );
              })}

              <AddList
                addList={addList}
                changeList={changeList}
                newListName={newListName}
              />
            </Box>
          </Container>
        </div>
      )}
    </div>
  );
};

export default AllList;
