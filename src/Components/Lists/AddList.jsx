import React, { useState } from 'react';
import {
    Box,
    Paper,
    TextField,
    Button,
    Typography,
    Container,
  } from "@mui/material";

  import CloseIcon from "@mui/icons-material/Close"

const AddList = ({changeList,addList,newListName}) => {
  const [show,setShow] = useState(false);
  return (
    <div className="add-list">
          {show ? (
            <div className="text-field">
              <TextField
              className='text-space'
              value={newListName}
                id="outlined-basic"
                label="Enter list title.."
                variant="outlined"
                
                onChange={(e) => changeList(e.target.value)}
              />
              <div className="btn">
                <Button
                  variant="contained"
                  sx={{ width: 100 }}
                  onClick={addList}
                >
                  Add List
                </Button>
                <CloseIcon
                sx={{}}
                  onClick={() => setShow(false)}
                  style={{color:"red", cursor: "pointer" }}
                />
              </div>
            </div>
          ) : (
            <Button variant="contained" onClick={()=>setShow(true)}>
              + Add Another list
            </Button>
          )}
        </div>
  )
}

export default AddList
