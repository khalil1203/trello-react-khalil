import { Box, Paper, TextField, Button, Typography } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import authorization from "../../Authorization/ApiKey";

const Allcards = ({ board, setBoard }) => {
  const [name, setName] = useState("");

  function changeName(e) {
    setName(e);
  }

  async function createCard() {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/boards/?name=${name}&key=${authorization.key}&token=${authorization.token}`
      );
    //   axios.put(`https://api.trello.com/1/boards/${response.data.id}?key=APIKey&token=APIToken`),{
    //     backgroundImage: "https://trello-backgrounds.s3.amazonaws.com/SharedBackground/original/4347627897ecb5a9799999d3325bbc2a/photo-1690184432588-81068877d852"
    //   }
      setBoard([...board, response.data]);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <Box sx={{ display: "flex", flexWrap: "wrap", gap: 10, py: 5 }}>
      {board.map((entry, index) => {
        return (
          <Link to={`/boards/${entry.id}`} key={entry.id}>
            <Paper
              sx={{
                p:2,
                display: `flex`,
                height: 160,
                width: 280,
                backgroundImage: `url(${entry.prefs.backgroundImage})`,
                backgroundSize: `cover`,
                color: "white",
                bgcolor: "darkblue"
              }}
              key={entry.id}
            >
              {entry.name}
            </Paper>
          </Link>
        );
      })}
      <Paper
        sx={{
          p: 1,
          display: `flex`,
          flexDirection: "column",
          height: 160,
          width: 280,
          bgcolor: "gray",
          backgroundSize: `cover`,
          textAlign: "center",
          alignItems:"center",
          gap:1
        }}
      >
        <TextField
          id="outlined-basic"
          label="Name of your Card..."
          variant="outlined"
          onChange={(e) => changeName(e.target.value)}
        />
        <Button sx={{bgcolor:"darkblue",width:150}} onClick={createCard}>Create Card</Button>
        <Typography>{10 - board.length} cards left</Typography>
      </Paper>
    </Box>
  );
};

export default Allcards;
