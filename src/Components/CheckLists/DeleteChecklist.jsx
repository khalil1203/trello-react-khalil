import * as React from "react";
import Box from "@mui/material/Box";
import { Button, Modal } from "@mui/material";

export default function DeleteChecklist({ deleteChecklistFunc, checklistId }) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  return (
    <div>
      <Box
        aria-describedby={id}
        onClick={handleClick}
        style={{ cursor: "pointer" }}
      >
        <b className="delete">Delete</b>
      </Box>
      <Modal
        id={id}
        open={open}
        anchorel={anchorEl}
        sx={{ width: 380, textAlign: "center" }}
        className="delete-checklist"
      >
        <Box
          sx={{
            border: 1,
            p: 1,
            bgcolor: "background.paper",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            gap: 1,
          }}
        >
          <h3>
            This will permanently delete this checklist. Do you want to proceed?
          </h3>
          <Button
            variant="contained"
            sx={{ p: 1 }}
            onClick={() => deleteChecklistFunc(checklistId)}
          >
            Yes
          </Button>
          <Button variant="text" sx={{ p: 1 }} onClick={handleClick}>
            No
          </Button>
        </Box>
      </Modal>
    </div>
  );
}
