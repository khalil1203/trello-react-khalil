import * as React from "react";
import { useState } from "react";
import { Modal, Typography, Box } from "@mui/material";
import AddChecklist from "./AddChecklist";
import authorization from "../../Authorization/ApiKey";
import axios from "axios";
import DeleteChecklist from "./DeleteChecklist";
import "./checklist.css";
import Items from "../Items/Items";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function CheckList({ name, cardId }) {
  const [checklist, setChecklist] = useState([]);
  const [checklistName, setChecklistName] = useState("");
  const [clicked,setClicked] = useState(false);

  function changeChecklistName(e) {
    setChecklistName(e);
  }

  async function AddCheckListFunc() {
    if (checklistName.length) {
      try {
        const response = await axios.post(
          `https://api.trello.com/1/cards/${cardId}/checklists?key=${authorization.key}&token=${authorization.token}`,
          { name: checklistName }
        );
        setChecklistName("");
        setChecklist([...checklist, response.data]);
      } catch (error) {
        console.error("cannot add checklist!!!", error);
      }
    }
  }

  async function deleteChecklistFunc(checklistId) {
    try {
      await axios.delete(
        `https://api.trello.com/1/cards/${cardId}/checklists/${checklistId}?key=${authorization.key}&token=${authorization.token}`
      );
      setChecklist(checklist.filter((list) => list.id !== checklistId));
    } catch (e) {
      console.error("Failed to Delete Checklist", e);
    }
  }

 async function getChecklist(){

 
    try {
      axios
        .get(
          `https://api.trello.com/1/cards/${cardId}/checklists?key=${authorization.key}&token=${authorization.token}`
        )
        .then((res) => {
          setChecklist(res.data);
        });
    } catch (error) {
      console.error("Failed to Get ALl Checklist", error);
    }
  };

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {setOpen(true),getChecklist()};
  const handleClose = () => setOpen(false);

  return (
    <div>
      <h3 onClick={handleOpen} style={{ cursor: "pointer" }}>
        {name}
      </h3>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          className="check-box"
          sx={style}
          style={{
            display: "flex",
            flexDirection: "column",
            gap: 10,
            minHeight: 600,
          }}
        >
          <Typography id="modal-modal-title" variant="h6" component="h2">
            <b>Card Title: {name.toUpperCase()}</b>
          </Typography>
          <AddChecklist
            checklistName={checklistName}
            AddCheckListFunc={AddCheckListFunc}
            changeChecklistName={changeChecklistName}
          />
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: 8,
              position: "absolute",
              marginTop: 20,
              width: 330,
            }}
          >
            {checklist.map((entry) => {
              return (
                <Box key={entry.id}>
                  <Box
                    key={entry.id}
                    sx={{
                      p: 1,
                      display: "flex",
                      justifyContent: "space-between",
                      borderRadius: 1,
                      bgcolor: "#F1F2F4",
                    }}
                  >
                    <Box>
                      {/* <CheckList name={entry.name} /> */}
                      <h3>{entry.name}</h3>
                    </Box>
                    <DeleteChecklist
                      deleteChecklistFunc={deleteChecklistFunc}
                      checklistId={entry.id}
                    />
                    {/* <CardDelete deleteCard={deleteCard} cardId = {entry.id}/> */}
                  </Box>

                  <Items checklistId={entry.id} checklist={entry} />
                </Box>
              );
            })}
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
