import React from "react";
import { useState } from "react";
import { TextField, Button } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

const AddChecklist = ({
  AddCheckListFunc,
  changeChecklistName,
  checklistName,
}) => {
  const [show, setShow] = useState(false);

  return (
    <div className="add-list">
      {show ? (
        <div className="text-field">
          <TextField
            id="outlined-basic"
            value={checklistName}
            label="Enter a title for Checklist..."
            variant="outlined"
            onChange={(e) => changeChecklistName(e.target.value)}
          />
          <div className="btn">
            <Button
              variant="contained"
              sx={{ width: 150 }}
              onClick={() => AddCheckListFunc()}
            >
              Add Checklist
            </Button>
            <CloseIcon
              onClick={() => setShow(false)}
              style={{ cursor: "pointer" }}
            />
          </div>
        </div>
      ) : (
        <Button variant="contained" onClick={() => setShow(true)}>
          + Add a Checklist
        </Button>
      )}
    </div>
  );
};

export default AddChecklist;
