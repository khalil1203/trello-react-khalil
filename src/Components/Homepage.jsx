import React from "react";
import AllBoards from "./AllBoards/AllBoards.jsx";
import { Container } from "@mui/material";
import Loader from "./Loading/Loader.jsx";

const Homepage = ({ board, setBoard }) => {
  if(!board.length){
    return <Loader/>
  }
  return (
    <div>
      <Container>
        <AllBoards board={board} setBoard={setBoard} />
      </Container>
    </div>
  );
};

export default Homepage;
