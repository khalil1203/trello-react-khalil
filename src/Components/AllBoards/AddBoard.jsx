import React, { useState } from "react";
import {
  Box,
  Paper,
  TextField,
  Button,
  Typography,
  Container,
} from "@mui/material";

import CloseIcon from "@mui/icons-material/Close";

const AddBoard = ({ changeName, createCard, board ,name}) => {
  const [show, setShow] = useState(false);
  return (
    <Paper
      sx={{
        p: 1,
        display: `flex`,
        flexDirection: "column",
        height: 150,
        width: 280,
        bgcolor: "gray",
        backgroundSize: `cover`,
        textAlign: "center",
        justifyContent: "space-between",
        alignItems: "center",
        gap: 1,
      }}
    >
      {show ? (
        <div className="text-field">
          <TextField
            id="outlined-basic"
            label="Enter Board title.."
            variant="outlined"
            value={name}
            onChange={(e) => changeName(e.target.value)}
          />
          <div className="btn" style={{ marginLeft: "2rem" }}>
            <Button
              variant="contained"
              sx={{ width: 150 }}
              onClick={createCard}
            >
              Add Board
            </Button>
            <CloseIcon
              onClick={() => setShow(false)}
              style={{ cursor: "pointer" }}
            />
          </div>
        </div>
      ) : (
        <Button
          variant="contained"
          sx={{ width: 200 }}
          onClick={() => setShow(true)}
        >
          + Add Another Board
        </Button>
      )}
      <Typography>{10 - board.length} cards left</Typography>
    </Paper>
  );
};

export default AddBoard;
