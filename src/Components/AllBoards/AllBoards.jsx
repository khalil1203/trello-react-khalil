import { Box, Paper, TextField, Button, Typography } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import authorization from "../../Authorization/ApiKey";
import AddBoard from "./AddBoard";

const AllBoards = ({ board, setBoard }) => {
  const [name, setName] = useState("");

  function changeName(e) {
    setName(e);
  }

  async function createCard() {
    if (board.length < 10 && name.length) {
      try {
        const response = await axios.post(
          `https://api.trello.com/1/boards/?name=${name}&key=${authorization.key}&token=${authorization.token}`
        );
        setBoard([...board, response.data]);
        setName("");
      } catch (error) {
        console.log(error);
      }
    }
  }

  return (
    <Box
      sx={{ display: "flex", flexWrap: "wrap", gap: 10, py: 5, bgcolor: "" }}
    >
      {board.map((entry, index) => {
        return (
          <Link to={`/boards/${entry.id}`} key={entry.id}>
            <Paper
              sx={{
                p: 2,
                display: `flex`,
                height: 150,
                width: 280,
                backgroundImage: `url(${entry.prefs.backgroundImage})`,
                backgroundSize: `cover`,
                color: "white",
                bgcolor: "darkblue",
              }}
              key={entry.id}
            >
              {entry.name}
            </Paper>
          </Link>
        );
      })}
      <AddBoard
        board={board}
        changeName={changeName}
        createCard={createCard}
        name={name}
      />
    </Box>
  );
};

export default AllBoards;
