import axios from "axios";
import React, { useEffect, useState } from "react";
import authorization from "../../Authorization/ApiKey";
import AddCard from "./AddCard";
import { Box } from "@mui/material";
import CardDelete from "./DeleteCard";
import CheckList from "../CheckLists/ChecklistModal";

const AllCards = ({ listid }) => {
  const [cards, setCards] = useState([]);

  async function deleteCard(e) {
    try {
      await axios.delete(
        `https://api.trello.com/1/cards/${e}?key=${authorization.key}&token=${authorization.token}`
      );
      setCards(cards.filter((card) => card.id !== e));
    } catch (e) {
      console.error("Failed to delete Card", e);
    }
  }

  useEffect(() => {
    try {
      axios
        .get(
          `https://api.trello.com/1/lists/${listid}/cards?key=${authorization.key}&token=${authorization.token}`
        )
        .then((res) => {
          setCards(res.data);
        });
    } catch (error) {
      console.log("error fetching cards of lists!!!!", error);
    }
  }, []);

  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: 1 }}>
      <Box sx={{ display: "flex", flexDirection: "column", gap: 1 }}>
        {cards.map((entry) => {
          return (
            <Box
              key={entry.id}
              sx={{
                p: 1,
                display: "flex",
                justifyContent: "space-between",
                borderRadius: 1,
                bgcolor: "#FFFFFF",
              }}
            >
              <Box>
                <CheckList  name={entry.name} cardId={entry.id} />
              </Box>

              <CardDelete deleteCard={deleteCard} cardId={entry.id} />
            </Box>
          );
        })}
      </Box>
      <AddCard cards={cards} setCards={setCards} listid={listid} />
    </Box>
  );
};

export default AllCards;
