import * as React from "react";
import Box from "@mui/material/Box";
import Popper from "@mui/material/Popper";
import CloseIcon from "@mui/icons-material/Close";
import { Button } from "@mui/material";

export default function CardDelete({ deleteCard, cardId }) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  return (
    <div>
      <i className="fa-solid fa-trash" aria-describedby={id}
        onClick={handleClick}
        style={{ cursor: "pointer" }}></i>
  
      {/* <CloseIcon
        
      /> */}
      <Popper id={id} open={open} anchorEl={anchorEl}>
        <Box
          sx={{ border: 1, p: 1, bgcolor: "background.paper", display: "flex",flexDirection:"column",justifyContent:"center",gap:1 }}
        >
          <h3>Do you want to delete this card?</h3>
          <Button variant="contained" sx={{p:1}}  onClick={() => deleteCard(cardId)}>
            Yes
          </Button>
          <Button variant="text" sx={{p:1}}  onClick={handleClick}>
            No
          </Button>
        </Box>
      </Popper>
    </div>
  );
}
