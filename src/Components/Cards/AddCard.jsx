import React, { useState } from "react";
import axios from "axios";
import { TextField, Button } from "@mui/material";

import CloseIcon from "@mui/icons-material/Close";
import authorization from "../../Authorization/ApiKey";

const AddCard = ({ cards, setCards, listid }) => {
  const [show, setShow] = useState(false);
  const [cstname, setName] = useState("");

  function changeCardName(e) {
    setName(e);
  }

  async function addCardOnList() {
    if (cstname.length) {
      try {
        const response = await axios.post(
          `https://api.trello.com/1/cards?idList=${listid}&key=${authorization.key}&token=${authorization.token}`,
          {
            name: cstname,
          }
        );
        setName("");
        setCards([...cards, response.data]);
      } catch (error) {
        console.log(error);
      }
    }
  }

  return (
    <div className="add-list">
      {show ? (
        <div className="text-field">
          <TextField
            id="outlined-basic"
            value={cstname}
            label="Enter a title for this card..."
            variant="outlined"
            onChange={(e) => changeCardName(e.target.value)}
          />
          <div className="btn">
            <Button
              variant="contained"
              sx={{ width: 120 }}
              onClick={() => addCardOnList()}
            >
              Add Card
            </Button>
            <CloseIcon
              onClick={() => setShow(false)}
              style={{ cursor: "pointer" }}
            />
          </div>
        </div>
      ) : (
        <Button variant="contained" onClick={() => setShow(true)}>
          + Add a card
        </Button>
      )}
    </div>
  );
};

export default AddCard;
